<%@ page import="domain.User" %>
<%@ page import="servlet.AdminServlet" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="domain.Product" %><%--
  Created by IntelliJ IDEA.
  User: haykazp
  Date: 13/01/2022
  Time: 07:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />

    <title>Project</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" />

    <link rel="stylesheet" href="http://localhost:8080/public/css/app.css" />
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="index3.html" class="brand-link">
            <img src="http://localhost:8080/public/images/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: 0.8;" />
            <span class="brand-text font-weight-light">Project</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="http://localhost:8080/public/images/profile.png" class="img-circle elevation-2" alt="User Image" />
                </div>
                <div class="info">
                    <a href="#" class="d-block">Username</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
       with font-awesome or any other icon font library -->
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link active">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Main
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="#" id="tab-1" class="nav-link active">
                                    <i class="fas fa-circle nav-icon"></i>
                                    <p>User</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" id="tab-2" class="nav-link">
                                    <i class="fas fa-circle nav-icon"></i>
                                    <p>Product</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Simple Link
                                <span class="right badge badge-danger">New</span>
                            </p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Starter Page</h1>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Starter Page</li>
                        </ol>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="tab-content-1">
                    <h1 class="m-0 text-dark">User List</h1>
                    <table class="table table-dark">
                        <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Passord</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <% ArrayList<User> users = (ArrayList<User>) request.getAttribute("users"); %>
                        <% for (User user: users) { %>
                        <tr>
                            <%="<th scope=\"row\">" + user.getId() +"</th>"%>
                            <%="<th>" + user.getFirst_name() +"</th>"%>
                            <%="<th>" + user.getLast_name() +"</th>"%>
                            <%="<th>" + user.getEmail() +"</th>"%>
                            <%="<th>" + user.getPassword() +"</th>"%>
                            <%="<th><a href=\"#\" class=\"badge badge-warning\">Update</a><a href=\"\" class=\"badge badge-danger\">Delete</a></th>"%>
                        </tr>
                        <%}%>
                        </tbody>
                    </table>
                    <div class="content" style="padding: 30px 0;">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">
                                    <form class="form-inline">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <input id="first_name" type="text" class="form-control" name="first_name" placeholder="First Name" required  autofocus>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <input id="last_name" type="text" class="form-control" name="last_name" placeholder="Last Name" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <input id="email" type="email" class="form-control" name="email" placeholder="Email"  required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Password Repeat" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <button type="submit" class="btn btn-primary" style="margin: 0 25px;">Create</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="tab-content-2">
                    <h1 class="m-0 text-dark">Product List</h1>
                    <table class="table table-dark">
                        <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Count</th>
                            <th scope="col">Code</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <% ArrayList<Product> products = (ArrayList<Product>) request.getAttribute("products"); %>
                        <% for (Product product: products) { %>
                        <tr>
                            <%="<th scope=\"row\">" + product.getId() +"</th>"%>
                            <%="<th>" + product.getName() +"</th>"%>
                            <%="<th>" + product.getPrice() +"</th>"%>
                            <%="<th>" + product.getCount() +"</th>"%>
                            <%="<th>" + product.getCode() +"</th>"%>
                            <%="<th><a href=\"#\" class=\"badge badge-warning\">Update</a>" +
                                    "<a href=\"#\" class=\"badge badge-danger\">Delete</a></th>"%>

                        </tr>
                        <%}%>
                        </tbody>
                    </table>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content -->

        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
                Anything you want
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="http://localhost:8080/public/js/app.js"></script>
    <script src="http://localhost:8080/public/js/tabs.js"></script>
</body>
</html>

