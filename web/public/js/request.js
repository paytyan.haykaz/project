$(document).ready(function(){

    var employee_first_name = $('#employee_first_name').val();
    var employee_last_name = $('#employee_last_name').val();
    var employee_email = $('#employee_email').val();
    var employee_password = $('#employee_password').val();
    var employee_password_repeat = $('#employee_password_repeat').val();



    var jsonObject = {
        first_name: employee_first_name,
        last_name: employee_last_name,
        email: employee_email,
        password:employee_password,
        password_repeat:employee_password_repeat
    };


    $('#employee').submit(function(e){


        $.ajax({
            type: 'PUT',
            url: 'http://localhost:8080/register',
            data: JSON.stringify(jsonObject),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            success: function (data) {
                console.log("ok");
            }
        });

    });


});