package service.dao;

import domain.Product;
import domain.Transaction;
import domain.User;

import java.util.ArrayList;

public interface TransactionDao {

    Transaction getById(long id);
    ArrayList<Transaction> getAll();
    boolean create(User user, Product product);
    boolean update(User user, Product product,long id);
    boolean delete(long id);

}
